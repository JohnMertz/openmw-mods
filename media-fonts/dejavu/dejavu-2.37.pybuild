# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import shutil

from pybuild.info import PN, PV, P

from common.exec import Exec
from common.mw import MW, InstallDir


class Package(Exec, MW):
    NAME = "DejaVu Fonts"
    DESC = "A font family based upon Bitstream Vera"
    HOMEPAGE = "https://github.com/dejavu-fonts/dejavu-fonts"
    LICENSE = "DejaVu"
    KEYWORDS = "openmw tes3mp"
    MY_PV = PV.replace(".", "_")
    SRC_URI = f"""
        https://github.com/{PN}-fonts/{PN}-fonts/releases/download/version_{MY_PV}/{PN}-fonts-ttf-{PV}.zip
        -> {P}.zip
    """
    S = f"{P}/dejavu-fonts-ttf-{PV}"
    # Needs to show up in the VFS for the font to be found
    INSTALL_DIRS = [InstallDir(".")]
    IUSE = "+minimal"

    def src_prepare(self):
        if "minimal" in self.USE:
            # Only install dejavu sans mono and Dejavu Sans and Dejavu Sans Serif
            os.makedirs("Fonts")
            for file in ["DejaVuSansMono.ttf", "DejaVuSans.ttf", "DejaVuSerif.ttf"]:
                os.rename(os.path.join("ttf", file), os.path.join("Fonts", file))
            shutil.rmtree("ttf")
        else:
            os.rename("ttf", "Fonts")
