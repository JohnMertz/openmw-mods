Title: Mod Updates
Author: Benjamin Winger <bmw@disroot.org>
Posted: 2023-02-15
Revision: 1
News-Item-Format: 2.0
Body: |-
  I would like to announce that I'm no longer going to be personally updating
  or adding new mods to the openmw package repository. This is something which has
  been the case for some time already as I've spent less time on that and more time
  dealing with issues and improving Portmod itself.

  This has been some time coming, but I wanted to make some improvements to the
  documentation before making this announcement.

  While I probably could continue as before, the reality has always been that
  Portmod's long-term survival is tied to its community, or lack thereof.
  Portmod is a tool which above all is meant to reduce the amount of time
  required to deal with mod installation and compatibility issues.
  However someone still always needs to spend some time getting things to
  work the first time, and the more people involved, the less any individual
  has to do. For the health of Portmod's repositories long-term, it would be
  better to think of Portmod as a collaborative tool for sharing mod
  installation and configuration details, rather than a tool for installing
  mods which other people have packaged.

  To make things clear, this is really just a shift in attitude. The only
  change to portmod which will occur, if any, is to clarify certain things
  in its documentation. I'm not going to start restricting features to
  contributors or anything like that; portmod will always be completely Free
  and Open Source software.

  I would also like to emphasise that a long-term commitment is not necessary
  when contributing. One-time contributions updating or adding packages are
  always welcome.

  Details about contributing to the openmw mod package repository can be found
  at https://gitlab.com/portmod/openmw-mods/-/blob/master/CONTRIBUTING.md.

  I also understand that contribution is probably more difficult than it could
  be, and is often hindered by confusing or missing documentation and the lack
  of tools to simplify things, even if it's not clear what exactly those tools
  should be. As such, I would like to also extend an appeal for contributions
  to Portmod's documentation, even if they are as simple as reporting where
  documentation is lacking. Documentation issues for Portmod's main documentation
  (https://portmod.readthedocs.io/en/stable/) can be opened on the main issue
  tracker, and the wiki has its own issue tracker
  https://gitlab.com/portmod/portmod-wiki/-/issues (rarely used so far).

  I will also try to reach out and advertise portmod a little more, as a larger
  community would also help with keeping things maintained. Help with that would
  also be appreciated.
