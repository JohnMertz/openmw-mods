# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild.info import P

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "Magicka Based Skill Progression"
    DESC = "Makes magical skill progression based on the amount of magicka used"
    HOMEPAGE = """
        ncgd? ( https://www.nexusmods.com/morrowind/mods/44973 )
        !ncgd? ( https://web.archive.org/web/20161103115849/http://mw.modhistory.com/download--12364 )
    """
    LICENSE = "attribution"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        ncgd? ( gameplay-advancement/ncgd )
    """
    KEYWORDS = "openmw"
    SRC_URI = f"""
        !ncgd? ( https://web.archive.org/web/20161103115849/http://mw.modhistory.com/file.php?id=12364 -> {P}.7z )
        ncgd? ( mbsp-ncgd-2.1b.7z )
    """
    IUSE = "ncgd"

    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[
                File(
                    "MBSP ncgdMW edit.omwaddon",
                    OVERRIDES="ncgdMW.omwaddon ncgdMW_alt_start.omwaddon",
                )
            ],
            REQUIRED_USE="ncgd",
            S="mbsp-ncgd-2.1b",
        ),
        InstallDir(".", PLUGINS=[File("MBSPv21.esp")], REQUIRED_USE="!ncgd", S=P),
    ]
