# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Vivec and Velothi - Arkitektora Vol.2"
    DESC = "Textures of Vivec City and Velothi Architecture"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46266"
    KEYWORDS = "openmw"
    LICENSE = "attribution"
    TEXTURE_SIZES = "1024 2048 4096"
    NEXUS_SRC_URI = """
        texture_size_1024? (
            https://www.nexusmods.com/morrowind/mods/46266?tab=files&file_id=1000012963
            -> Vivec_and_Velothi_Arkitektora__-_MQ-46266-2-0-1546272664.rar
        )
        texture_size_2048? (
            https://www.nexusmods.com/morrowind/mods/46266?tab=files&file_id=1000012962
            -> Vivec_and_Velothi_Arkitektora__-_HQ-46266-2-0-1546272606.rar
        )
        texture_size_4096? (
            https://www.nexusmods.com/morrowind/mods/46266?tab=files&file_id=1000012962
            -> Vivec_and_Velothi_Arkitektora__-_HQ-46266-2-0-1546272606.rar
        )
        atlas? (
            texture_size_1024? (
                https://www.nexusmods.com/morrowind/mods/46266?tab=files&file_id=1000012965
                -> Vivec_and_Velothi_Arkitektora__-_ATLAS_MQ-46266-1-0-1546272827.rar
            )
            texture_size_2048? (
                https://www.nexusmods.com/morrowind/mods/46266?tab=files&file_id=1000012964
                -> Vivec_and_Velothi_Arkitektora__-_ATLAS_HQ-46266-1-0-1546272773.rar
            )
            texture_size_4096? (
                https://www.nexusmods.com/morrowind/mods/46266?tab=files&file_id=1000012966
                -> Vivec_and_Velothi_Arkitektora__-_ATLAS_UHQ-46266-1-0-1546273178.rar
            )
        )
        map_normal? (
            https://www.nexusmods.com/morrowind/mods/46398?tab=files&file_id=1000013519
            -> Vivec_and_Velothi_Arkitektora_Normal_Maps_-_HQ-46398-1-0-1550546387.rar
            atlas? (
                texture_size_1024? (
                    https://www.nexusmods.com/morrowind/mods/46398?tab=files&file_id=1000013529
                    ->  Vivec_and_Velothi_Arkitektora_-_ATLAS_MQ-46398-1-0-1550635606.rar
                )
                texture_size_2048? (
                    https://www.nexusmods.com/morrowind/mods/46398?tab=files&file_id=1000013522
                    -> Vivec_and_Velothi_Arkitektora_-_ATLAS_Normals_HQ-46398-1-1-1550569464.rar
                )
                texture_size_4096? (
                    https://www.nexusmods.com/morrowind/mods/46398?tab=files&file_id=1000013530
                    -> Vivec_and_Velothi_Arkitektora_-_ATLAS_Normals_UHQ-46398-1-0-1550637845.rar
                )
            )
        )
    """
    IUSE = "atlas map_normal"
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            S="Vivec_and_Velothi_Arkitektora__-_MQ-46266-2-0-1546272664",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            "Data Files",
            S="Vivec_and_Velothi_Arkitektora__-_HQ-46266-2-0-1546272606",
            REQUIRED_USE="|| ( texture_size_2048 texture_size_4096 )",
        ),
        InstallDir(
            "ATL MQ",
            S="Vivec_and_Velothi_Arkitektora__-_ATLAS_MQ-46266-1-0-1546272827",
            REQUIRED_USE="texture_size_1024 atlas",
        ),
        InstallDir(
            "ATL HQ",
            S="Vivec_and_Velothi_Arkitektora__-_ATLAS_HQ-46266-1-0-1546272773",
            REQUIRED_USE="texture_size_2048 atlas",
        ),
        # Author notes that this UHQ atlas texture is very demanding on most graphics
        # cards however it is unknown if that is a problem for openmw as much as it is
        # for morrowind.
        InstallDir(
            "Data Files",
            S="Vivec_and_Velothi_Arkitektora__-_ATLAS_UHQ-46266-1-0-1546273178",
            REQUIRED_USE="texture_size_4096 atlas",
        ),
        InstallDir(
            "Vivec and Velothi Arkitektora  - HQ Normals/Data Files",
            S="Vivec_and_Velothi_Arkitektora_Normal_Maps_-_HQ-46398-1-0-1550546387",
            REQUIRED_USE="map_normal",
        ),
        InstallDir(
            "Vivec and Velothi Arkitektora  - ATLAS MQ/ATL MQ",
            S="Vivec_and_Velothi_Arkitektora_-_ATLAS_MQ-46398-1-0-1550635606",
            REQUIRED_USE="texture_size_1024 atlas map_normal",
        ),
        InstallDir(
            "Vivec and Velothi Arkitektora - ATLAS Normals HQ/ATL HQ",
            S="Vivec_and_Velothi_Arkitektora_-_ATLAS_Normals_HQ-46398-1-1-1550569464",
            REQUIRED_USE="texture_size_2048 atlas map_normal",
        ),
        InstallDir(
            "Vivec and Velothi Arkitektora  - ATLAS UHQ/Data Files",
            S="Vivec_and_Velothi_Arkitektora_-_ATLAS_Normals_UHQ-46398-1-0-1550637845",
            REQUIRED_USE="texture_size_4096 atlas map_normal",
        ),
    ]
