# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Vurts Groundcover"
    DESC = "Vurts Groundcover patched for OpenMW."
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45464"
    KEYWORDS = "openmw"
    LICENSE = "CC-BY-NC-4.0"
    NEXUS_URL = HOMEPAGE
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        solstheim? (
            !!base/tomb-of-the-snow-prince[grass]
            !!land-flora/ozzy-grass[bloodmoon]
            !!land-flora/remiros-groundcover[solstheim]
            !!land-flora/aesthesia-groundcover[bloodmoon]
        )
        !minimal? (
            !!land-flora/aesthesia-groundcover
            !!land-flora/remiros-groundcover
            !!land-flora/ozzy-grass
        )
    """
    TEXTURE_SIZES = "1024"
    SRC_URI = "Vurts_Groundcover_v2.3_for_OpenMW-45464-1-0.7z"
    IUSE = "+solstheim minimal"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            GROUNDCOVER=[
                File("Vurt's Groundcover - Solstheim.esp", REQUIRED_USE="solstheim"),
                File(
                    "Vurt's Groundcover - BC, AI, WG, GL.esp", REQUIRED_USE="!minimal"
                ),
                File("Vurt's Groundcover - The Ashlands.esp", REQUIRED_USE="!minimal"),
                File("Vurt's Groundcover - Reeds.esp"),
            ],
        )
    ]
