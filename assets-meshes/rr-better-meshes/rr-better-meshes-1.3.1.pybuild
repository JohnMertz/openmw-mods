# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.atlasgen import AtlasGen
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(AtlasGen, NexusMod, MW):
    NAME = "RR Mod Series - Better Meshes"
    DESC = "Properly smoothed vanilla meshes"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43266"
    LICENSE = "attribution"
    RDEPEND = "base/morrowind"
    DEPEND = "virtual/imagemagick"
    KEYWORDS = ""
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/43266"
    SRC_URI = """
        RR_-_Better_Meshes_1.3.1-43266-1-3-1-1569071188.rar
        redoran-architecture? (
            Better_Redoran_Architecture_V1.2.2-43266-1-2-2-1565704943.rar
        )
        books? ( RR_-_Better_Books_1.0-43266-1-0-1565799800.rar )
        frescoes? ( RR_-_Better_Frescoes_V1.1-43266-1-1-1565882616.rar )
        crates-barrels? ( RR_-_Better_Crates_and_Barrels-43266-1-1-1573844992.zip )
        crystals? ( RR_-_Better_Crystals_1.0-43266-1-0-1575647179.rar )
        ashlands-overhaul? (
            RR_-_Vurt_Ashtrees_Remastered_V1.2.1-43266-1-2-1-1579374300.zip
        )
        devtools? ( Book_Pages_Assets-43266-1-0.rar )
        bones? ( RR_-_Better_Skulls_and_Bones-43266-1-0-1579298110.rar )
    """
    IUSE = """
        devtools
        +skooma-pipe-1
        skooma-pipe-2
        skooma-pipe-3
        +potions
        +books
        +frescoes
        +redoran-architecture
        gitd
        +ashlands-overhaul
        +crates-barrels
        +crystals
        +bones
        l10n_ru
    """
    TIER = 1
    REQUIRED_USE = "?? ( skooma-pipe-1 skooma-pipe-2 skooma-pipe-3 )"
    TEXTURE_SIZES = "1024 2048"

    INSTALL_DIRS = [
        InstallDir(
            "00 - Main Files", S="RR_-_Better_Meshes_1.3.1-43266-1-3-1-1569071188"
        ),
        InstallDir(
            "01 - Optional - 0.5 Potions",
            S="RR_-_Better_Meshes_1.3.1-43266-1-3-1-1569071188",
            REQUIRED_USE="potions",
        ),
        InstallDir(
            "02 - Optional - Skooma Pipe 1",
            S="RR_-_Better_Meshes_1.3.1-43266-1-3-1-1569071188",
            REQUIRED_USE="skooma-pipe-1",
        ),
        InstallDir(
            "03 - Optional - Skooma Pipe 2",
            S="RR_-_Better_Meshes_1.3.1-43266-1-3-1-1569071188",
            REQUIRED_USE="skooma-pipe-2",
        ),
        InstallDir(
            "04 - Optional - Skooma Pipe 3",
            S="RR_-_Better_Meshes_1.3.1-43266-1-3-1-1569071188",
            REQUIRED_USE="skooma-pipe-3",
        ),
        InstallDir(
            "05 - Russian - Apparatus Fix",
            PLUGINS=[File("RR_Apparatus_Fix_1C.ESP")],
            S="RR_-_Better_Meshes_1.3.1-43266-1-3-1-1569071188",
            REQUIRED_USE="l10n_ru",
        ),
        InstallDir(
            "00 - Vanilla",
            S="Better_Redoran_Architecture_V1.2.2-43266-1-2-2-1565704943",
            REQUIRED_USE="redoran-architecture",
        ),
        InstallDir(
            "01 - Vanilla GITD Patch",
            S="Better_Redoran_Architecture_V1.2.2-43266-1-2-2-1565704943",
            REQUIRED_USE="redoran-architecture gitd",
        ),
        InstallDir(
            "02 - Atlas",
            ATLASGEN=[File("RR_redoran_atlas_generator.bat")],
            S="Better_Redoran_Architecture_V1.2.2-43266-1-2-2-1565704943",
            REQUIRED_USE="redoran-architecture",
        ),
        InstallDir(
            "03 - Atlas GITD patch",
            S="Better_Redoran_Architecture_V1.2.2-43266-1-2-2-1565704943",
            REQUIRED_USE="redoran-architecture gitd",
        ),
        InstallDir(
            "04 - Better Aldruhn-under-Skar",
            PLUGINS=[
                File("RR_Aldruhn_Under_Skar_Rus.ESP", REQUIRED_USE="l10n_ru"),
                File("RR_Aldruhn_Under_Skar_Eng.ESP", REQUIRED_USE="!l10n_ru"),
            ],
            S="Better_Redoran_Architecture_V1.2.2-43266-1-2-2-1565704943",
            REQUIRED_USE="redoran-architecture",
        ),
        InstallDir(
            "05 - Better Aldruhn-under-Skar HD",
            S="Better_Redoran_Architecture_V1.2.2-43266-1-2-2-1565704943",
            REQUIRED_USE="texture_size_2048 redoran-architecture",
        ),
        InstallDir(
            ".",
            S="RR_-_Better_Books_1.0-43266-1-0-1565799800",
            REQUIRED_USE="books",
        ),
        InstallDir(
            ".",
            S="RR_-_Better_Frescoes_V1.1-43266-1-1-1565882616",
            REQUIRED_USE="frescoes",
        ),
        InstallDir(
            ".",
            S="RR_-_Better_Crates_and_Barrels-43266-1-1-1573844992",
            REQUIRED_USE="crates-barrels",
        ),
        InstallDir(
            ".",
            S="RR_-_Better_Crystals_1.0-43266-1-0-1575647179",
            REQUIRED_USE="crystals",
        ),
        InstallDir(
            "00 - Main Files",
            S="RR_-_Vurt_Ashtrees_Remastered_V1.2.1-43266-1-2-1-1579374300",
            REQUIRED_USE="ashlands-overhaul",
        ),
        # InstallDir(
        #     "01 - Optional - Fire Tree Without Fire",
        #     S="RR_-_Vurt_Ashtrees_Remastered_V1.2.1-43266-1-2-1-1579374300",
        #     REQUIRED_USE="ashlands-overhaul",
        # ),
        InstallDir(
            "00 - Main Files",
            S="RR_-_Better_Skulls_and_Bones-43266-1-0-1579298110",
            REQUIRED_USE="bones",
        ),
        InstallDir(
            "01 - Main ESP - English",
            S="RR_-_Better_Skulls_and_Bones-43266-1-0-1579298110",
            REQUIRED_USE="bones !l10n_ru",
        ),
        InstallDir(
            "02 - Main ESP - Russian",
            S="RR_-_Better_Skulls_and_Bones-43266-1-0-1579298110",
            REQUIRED_USE="bones l10n_ru",
        ),
        InstallDir(".", S="Book_Pages_Assets-43266-1-0", REQUIRED_USE="devtools"),
    ]
