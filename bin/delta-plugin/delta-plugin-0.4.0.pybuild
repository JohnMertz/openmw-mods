# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from pybuild.info import P

from common.exec import Exec


class Package(Exec):
    NAME = "DeltaPlugin"
    DESC = "A tool for handling and converting markup-based versions of esp files"
    HOMEPAGE = "https://gitlab.com/bmwinger/delta-plugin"
    SRC_URI = f"""
        platform_linux? (
            https://gitlab.com/bmwinger/delta-plugin/-/package_files/7926053/download
            -> {P}-linux-amd64.zip
        )
        platform_win32? (
            https://gitlab.com/bmwinger/delta-plugin/-/package_files/7926054/download
            -> {P}-windows-amd64.zip
        )
    """
    LICENSE = "GPL-3"
    KEYWORDS = "openmw"
    S = f"{P}-linux-amd64"
    IUSE = "platform_linux platform_win32"

    def src_prepare(self):
        super().src_prepare()
        os.makedirs("bin")
        exe_name = (
            "delta_plugin.exe" if "platform_win32" in self.USE else "delta_plugin"
        )
        path = os.path.join(self.WORKDIR, self.S, "bin", exe_name)
        os.rename(exe_name, path)
